#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    char filename[] = "fifa-player-stats-database";
    char zipname[] = "fifa-player-stats-database.zip";
    char target[] = "FIFA23_official_data.csv";
    char command[BUFSIZ];

    // ? Download Database
    if (access(zipname, F_OK) == 0) {
        printf("%s already exist\n", zipname);
    } else {
        sprintf(command, "kaggle datasets download -d bryanb/%s", filename);
        system(command);
    }

    // ? Unzip Downloaded zip
    if (access(target, F_OK) == 0) {
        printf("%s already exist\n", target);
    } else {
        sprintf(command, "unzip %s.zip", filename);
        system(command);
    }

    // ? AWK Data
    sprintf(command,
            "awk -F ',' '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {"
            "print \"Name: \" $2;"
            "print \"Age: \" $3;"
            "print \"Club: \" $9;"
            "print \"Nationality: \" $5;"
            "print \"Potential: \" $8;"
            "print \"Wage: \" $12;"
            "print \"Photo: \" $4;"
            "print \"--------------------------\";"
            "}' %s", target);
    system(command);

    return 0;
}