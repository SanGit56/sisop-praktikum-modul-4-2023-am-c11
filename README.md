# Modul 4

## Anggota

- Irsyad Fikriansyah Ramadhan (5025211149)
- Ligar Arsa Arnata (5025211244)
- Radhiyan Muhammad Hisan (5025211166)

## Penjelasan

### soal1

1.a Menggunakan Kaggle API untuk men-download data base yang tertera pada soal. Dapat dilakukan dengan command `kaggle datasets download -d <link>` yang kemudian dipassing menggunakan fungsi system() pada c. Kemudian unzip menggunakan system()

```c
    char filename[] = "fifa-player-stats-database";
    char zipname[] = "fifa-player-stats-database.zip";
    char target[] = "FIFA23_official_data.csv";
    char command[BUFSIZ];

    // ? Download Database
    if (access(zipname, F_OK) == 0) {
        printf("%s already exist\n", zipname);
    } else {
        sprintf(command, "kaggle datasets download -d bryanb/%s", filename);
        system(command);
    }

    // ? Unzip Downloaded zip
    if (access(target, F_OK) == 0) {
        printf("%s already exist\n", target);
    } else {
        sprintf(command, "unzip %s.zip", filename);
        system(command);
    }
```

1.b Untuk menampilkan data yang diinginkan dengan mudah, digunakan command `awk`

```c
    char target[] = "FIFA23_official_data.csv";

    // ? AWK Data
    sprintf(command,
            "awk -F ',' '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {"
            "print \"Name: \" $2;"
            "print \"Age: \" $3;"
            "print \"Club: \" $9;"
            "print \"Nationality: \" $5;"
            "print \"Potential: \" $8;"
            "print \"Wage: \" $12;"
            "print \"Photo: \" $4;"
            "print \"--------------------------\";"
            "}' %s", target);
    system(command);
```

1.c Membuat dockerfile untuk membuat image dengan menuliskan langkah-langkah yang perlu dilakukan untuk meng-compile program storage.c. namun sebelumnya, perlu dicopy kaggle.json pada current working directory untuk di copy kedalam docker image yang akan dibuat

```dockerfile
# Specify the base image
FROM gcc:latest

# Install Python and create a virtual environment
RUN apt-get update && \
    apt-get install -y python3 python3-pip python3-venv && \
    python3 -m venv /venv

# Activate the virtual environment
ENV PATH="/venv/bin:$PATH"

# Install the Kaggle CLI within the virtual environment
RUN pip3 install kaggle

# Set the working directory inside the container
WORKDIR /app

# Copy the source code to the container's working directory
COPY storage.c .

# Copy the kaggle.json file to the container
COPY kaggle.json /root/.kaggle/kaggle.json

# Set the permissions for the kaggle.json file
RUN chmod 600 /root/.kaggle/kaggle.json

# Compile the C code
RUN gcc -o storage storage.c

# Set the command to run when the container starts
CMD ["./storage"]
```

setelah docker image berhasil dibuat, hasil docker image dapat dilihat dengan menjalankan `docker run <nama image>` untuk membuat sebuah container

![1c](images/1c.jpg)

1.d setelah membuat docker hub, sebelum push image yang telah dibuat ke docker hub, perlu dilakukan tag pada image dengan link dockerhub `docker tag <image>:<tag> <username>/<repository>:<tag>`

setelah itu login mengunkan `docker login`. Terakhir, kita dapat push image yang telah dibuat

1.e berikut docker-compose.yml yang kami buat untuk membuat 5 instance

```docker
version: '3'
services:
  myservice:
    image: irsyadfikriansyah/storage-app:v2.0
    deploy:
      replicas: 5
```

setelah dibuat, dapat di-copy file tersebut pada masing-masing directory Barcelona dan Napoli dan kemudian dijalankan `docker-compose up -d` pada directory tersebut

![1e](images/1e.jpg)

### soal3

Belum ada permintaan yang terpenuhi

### soal4

![Gambar 4](images/4a.png)

Pada soal nomor 4, kita harus menentukan directory path yang nantinya akan dijadikan base untuk folder yang menjadi mount point. Path ini akan disimpan dalam variabel dirPath, yang mana pada code diatas telah ditentukan yaitu pada "home/ligar/Test". Dimana pada folder Test ini nantinya terdapat 2 subfolder yaitu Txt dan Cobacoba. Folder Txt sendiri berisikan file.txt yang telah dituliskan beberapa kalimat yang membuat size dari file.txt cukup untuk melalui proses modularisasi.

![Gambar 4](images/4b.png)

Setelah membuat dirPath, ada function yang bernama decode. Function ini berguna untuk melakukan modularisasi pada folder yang sesuai dengan beberapa aturan pada function ini. Pada function ini akan dilakukan pengcekkan size file untuk mengetahui apakah file tersebut cukup besar untuk dimodularisasi atau tidak. Selain itu juga akan dilakukan pengecekkan nama folder yang mana jika sebuah folder memiliki awalan atau prefix "module_" maka isi dari folder tersebut akan dimodularisasi. Selain pengecekkan aturan, pada fungsi ini juga dilakuan penamaan file file yang telah dimodularisasi dengan nama yang sesuai dengan perintah soal. 

![Gambar 4](images/4c.png)

Selanjutnya adalah function encrypt, Function ini berguna untuk menyatukan kembali file file yang telah dimodularisasi. Seperti jika ada suatu folder yang berawalan "module_" dan didalamnya memiliki file yang telah dimodularisasi maka jika folder tersebut direname ulang, semua file yang telah dimodularisasi akan kemabali pada wujud semulanya seperti sebelum dimodularisasi. Setelah fungsi encrypt, ada banyak fungsi pengimlemntasian FUSE seperti get attribute, fungsi untuk membaca folder, menghapus folder, membuat folder, dan merename folder. Berikut merupakan contoh perjalanan pada soal no 4.

![Gambar 4](images/4d.png)

Gambar diatas merupakan isi file.txt yang ada pada subfolder Txt di folder utama Test yang telah di mount.

![Gambar 4](images/4f.png)

Gambar diatas merupakan step dalam merename subfolder Txt menjadi "module_Txt" sehingga file.txt didalamnya akan termodularisasi.

![Gambar 4](images/4e.png)

Setelah subfolder direname, maka file.txt akan terpecah seperti pada gambar diatas.

![Gambar 4](images/4g.png)

Namun setelah subfolder "moduler_Txt" di rename ulang, maka isi file.txt akan berubah menjadi semula seperti pada gambar diatas.

![Gambar 4](images/4h.png)

Seluruh perubahan yang terjadi pada folder Test akan tercatat pada file fs_module.log yang tersimpan di home.

## kendala

### no1

- menurut kami soal point terakhir cukup tidak jelas, sehingga kami tidak tahu apa yang diinginkan dari soal tersebut.
- tidak cukup dijelaskan sebelumnya bagaimana cara membuat Dockerfile dan docker-compose.yml, terutama pada syntax dan fungsi tiap kata.

### no3

- Jika ada fungsi `open()', saat di-mount, file-file dalam direktori hasil mount menjadi file binary
- Filesystem error (*transport endpoint is not connnected*) saat melakukan `fopen()`

### no4

- Selesai pada waktu Revisi
