#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

static  const  char *dirpath = "/home/hisan/Documents/Sisop/Sisop_230527_Shift4/soal3/inifolderetc";

char* enkode_base64(const unsigned char* masukan, size_t pjg_masukan) {
    const char tabel_base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    size_t pjg_terenkode = ((pjg_masukan + 2) / 3) * 4;
    char* data_terenkode = (char*)malloc(pjg_terenkode + 1);
    size_t i, j;

    if (data_terenkode == NULL) {
        return NULL;
    }

    for (i = 0, j = 0; i < pjg_masukan; i += 3, j += 4) {
        unsigned int oktet_a = masukan[i];
        unsigned int oktet_b = (i + 1 < pjg_masukan) ? masukan[i + 1] : 0;
        unsigned int oktet_c = (i + 2 < pjg_masukan) ? masukan[i + 2] : 0;

        data_terenkode[j] = tabel_base64[oktet_a >> 2];
        data_terenkode[j + 1] = tabel_base64[((oktet_a & 3) << 4) | (oktet_b >> 4)];
        data_terenkode[j + 2] = tabel_base64[((oktet_b & 15) << 2) | (oktet_c >> 6)];
        data_terenkode[j + 3] = tabel_base64[oktet_c & 63];
    }

    // Add padding characters if necessary
    switch (pjg_masukan % 3) {
        case 1:
            data_terenkode[pjg_terenkode - 2] = '=';
            data_terenkode[pjg_terenkode - 1] = '=';
            break;
        case 2:
            data_terenkode[pjg_terenkode - 1] = '=';
            break;
    }

    data_terenkode[pjg_terenkode] = '\0';

    return data_terenkode;
}

void baca_file(char* nama_file) {
    printf("baca %s\n", nama_file);

    FILE* file = fopen(nama_file, "rb");
    if (file == NULL) {
        printf("gagal membuka file\n");
    }

    fseek(file, 0, SEEK_END);
    long ukuran_file = ftell(file);
    fseek(file, 0, SEEK_SET);

    unsigned char* konten_file = (unsigned char*)malloc(ukuran_file);
    if (konten_file == NULL) {
        printf("gagal mengalokasikan memori\n");
        fclose(file);
    }

    fread(konten_file, 1, ukuran_file, file);
    char *terenkode = enkode_base64(konten_file, ukuran_file);

    fseek(file, 0, SEEK_SET);
    fwrite(terenkode, 1, strlen(terenkode), file);
    fflush(file);

    fclose(file);
    free(konten_file);
    free(terenkode);
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);
    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;
    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        char *garing_akhir = strrchr(fpath, '/');
        int idx_garing = garing_akhir - fpath;

        if (fpath[idx_garing+1] == 'L' || fpath[idx_garing+1] == 'l' ||
            fpath[idx_garing+1] == 'U' || fpath[idx_garing+1] == 'u' ||
            fpath[idx_garing+1] == 'T' || fpath[idx_garing+1] == 't' ||
            fpath[idx_garing+1] == 'H' || fpath[idx_garing+1] == 'h') {
                if (S_ISREG(st.st_mode)) {
                    // baca_file(de->d_name);

                    printf("baca %s\n", de->d_name);

                    FILE* file = fopen(de->d_name, "rb");
                    if (file == NULL) {
                        printf("gagal membuka file\n");
                    }

                    fseek(file, 0, SEEK_END);
                    long ukuran_file = ftell(file);
                    fseek(file, 0, SEEK_SET);

                    unsigned char* konten_file = (unsigned char*)malloc(ukuran_file);
                    if (konten_file == NULL) {
                        printf("gagal mengalokasikan memori\n");
                        fclose(file);
                    }

                    fread(konten_file, 1, ukuran_file, file);
                    char *terenkode = enkode_base64(konten_file, ukuran_file);

                    fseek(file, 0, SEEK_SET);
                    fwrite(terenkode, 1, strlen(terenkode), file);
                    fflush(file);

                    fclose(file);
                    free(konten_file);
                    free(terenkode);
                }
        }

        if(res!=0)
            break;
    }

    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;
    (void) fi;
    fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}